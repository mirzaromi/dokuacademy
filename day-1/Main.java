public class Main {
    public static void main(String[] args) {
        boolean prime = Main.isPrime(1000);
        if (prime) {
            System.out.println("Bilangan Prima");
        } else {
            System.out.println("Bukan Bilangan Prima");
        }
    }

    public static boolean isPrime(int num) {
        if (num<=1){
            return false;
        }

        for (int i=2; i*i<num; i++) {
            if (num%i==0) {
                return false;
            }
        }

        return true;
    }
}