import java.lang.Math;

public class Lamp {
    public static void main(String[] args) {
        boolean isLampOn = isOn(64);

        if (isLampOn) {
            System.out.println("Lampu Nyala");
        } else {
            System.out.println("Lampu Mati");
        }


    }

    public static boolean isOn(int num){
        double n = Math.floor(Math.sqrt(num));
        int newN = (int) n;

        if (newN*newN != num) {
            return false;
        }

        return true;
    }
}